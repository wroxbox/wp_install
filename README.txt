This is my installation shell script creating Wordpress on my local environments.

You may choose to download by GIT [1] or just the regular latest [2].

Script also creates the databases, user and grants right to use database.

Script will configure your wp-config.php with the credentials you inserted and
will also add the security salt-key [3]

Please feel free to fork it.

Follow me on twitter: wroxbox

[1] = git clone https://github.com/WordPress/WordPress.git
[2] = wget http://wordpress.org/latest.tar.gz
[3] = https://api.wordpress.org/secret-key/1.1/salt/
